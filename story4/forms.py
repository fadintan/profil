from django import forms
from story4 import models


years = [x for x in range(2019, 2025)]

class ScheduleForm(forms.Form):
    
    activity = forms.CharField(
        label = "Activity",
        max_length = 100,
        required = True,
        widget = forms.TextInput(attrs ={'class' : 'form-control form-control-sm', 'placeholder': 'Story 5 PPW'})
    )

    subject = forms.ChoiceField(
        label = 'Subject',
        choices=[
                ('PPW', 'PPW'),
                ('SDA', 'SDA'),
                ('FisDas', 'FisDas'),
                ('POK', 'POK'),
                ('Matdas2','Matdas2'),
                ('Others', 'Others')],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm', 'placeholder':'PPW'})
    )

    category = forms.ChoiceField(
        label = 'Category',
        choices=[('Assignment', 'Assignment'),
                ('Quiz', 'Quiz'),
                ('Lab', 'Lab'),
                ('Personal', 'Personal'),
                ('Organization', 'Organization')],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm'})
    )

    day = forms.ChoiceField(
        label = 'Day',
        choices=[('Sunday', 'Sunday'),
                ('Monday', 'Monday'),
                ('Tuesday', 'Tuesday'),
                ('Wednesday', 'Wednesday'),
                ('Thursday', 'Thursday'),
                ('Friday', 'Friday'),
                ('Saturday', 'Saturday')],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm'})
    )
    date = forms.DateField(
        label = "Date",
        widget = forms.SelectDateWidget(years = years, attrs = {'class' : 'form-control-sm'})
    )

    time = forms.TimeField(
        label = 'Time',
        input_formats = ['%H:%M'],
        widget = forms.TextInput(attrs={'class' : 'form-control form-control-sm', 'placeholder' : '00:00'}),
    )

    place = forms.CharField(
        label = "Place",
        max_length = 50,
        required = True,
        widget = forms.TextInput(attrs ={'class' : 'form-control form-control-sm', 'placeholder': 'Fasilkom UI'})
    )




