from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('info/', views.info, name='info'),
    path('playlist/', views.playlist, name='playlist'),
    path('schedule/', views.sched, name='schedule'),
]