from django.db import models

# Create your models here.
class schedule(models.Model):
    activity = models.CharField(max_length = 100)
    place = models.CharField(max_length = 50)
    day = models.CharField(max_length = 20)
    date = models.DateField()
    time = models.TimeField()
    category = models.CharField(max_length = 50)
    subject = models.CharField(max_length = 50)