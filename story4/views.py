from django.shortcuts import render, redirect
from story4 import forms, models
from .models import schedule
from datetime import datetime
from django.http import HttpResponseRedirect

# Create your views here.
def home(request):
    return render(request, "home.html", {})

def info(request):
    return render(request, "personalinfo.html", {})

def about(request):
    return render(request, "about.html", {})

def contact(request):
    return render(request, "contact.html", {})

def playlist(request):
    return render(request, "additional.html", {})

def sched(request):
    if request.method == "POST":
        form = forms.ScheduleForm(request.POST)
        if 'id' in request.POST:
            schedule.objects.get(id=request.POST['id']).delete()
            return redirect('/schedule/')

        if form.is_valid():
            sched = models.schedule(
                activity = form.data['activity'],
                place = form.data['place'],
                day = form.data['day'],
                time = form.data['time'],
                date = form.data['date_year'] + "-" + form.data['date_month'] + "-" + form.data['date_day'],
                category = form.data['category'],
                subject = form.data['subject'],
                )
            sched.save()
            return redirect('/schedule/')
    else:
        form = forms.ScheduleForm()

    schedule_context = {
        'schedule_active' : 'active',
        'context' : {'now' : datetime.now()},
        'schedule' : models.schedule.objects.all().values(),
        'form' : form,
    }

    return render(request, 'schedule.html', schedule_context)
